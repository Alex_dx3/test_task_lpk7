﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DotNetOpenAuth.OAuth2;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using test_task.Models;
using test_task.Models.Infrastructure;
using test_task.Services.Abstracts;

namespace test_task.Controllers
{
    [ApiController]
    [Route("api")]
    public class ApiController : ControllerBase
    {
        private IApiService apiService;
        public ApiController(IApiService apiService)
        {
            this.apiService = apiService;
        }

        [HttpGet("GetTemplates")]
        public async Task<BaseResponse> GetTemplates()
        {
            return await apiService.GetTemplateModelsAsync();
        }
        [HttpPost("CreateSite")]
        public async Task<BaseResponse> CreateSite([FromBody] SiteViewModel site)
        {
            return await apiService.CreateSiteAsync(site);
        }
    }
}
