using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using DotNetOpenAuth.OAuth2;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using test_task.Models;
using test_task.Models.Infrastructure;
using test_task.Services.Abstracts;

namespace test_task.Services
{
    public class ApiService : IApiService
    {
        private readonly string accessToken;
        private readonly IHttpClientFactory clientFactory;
        private AuthorizationServerDescription authorizationServer;
        private WebServerClient webServerClient;

        public ApiService(IHttpClientFactory clientFactory, IOptions<ApiOptions> apiOptions)
        {
            var option = apiOptions.Value;
            authorizationServer = new AuthorizationServerDescription()
            {
                TokenEndpoint = new Uri(option.TOKEN_ENDPOINT),
                AuthorizationEndpoint = new Uri(option.AUTHZ_ENDPOINT)
            };
            webServerClient = new WebServerClient(authorizationServer, option.clientID, option.clientSecret);

            var authorization = webServerClient.GetClientAccessToken(new[] { "Api.Access" });
            if (authorization == null)
                throw new Exception("Authorization failed");
            this.accessToken = authorization.AccessToken;

            this.clientFactory = clientFactory;
        }

        private void SetupClient(HttpClient client)
        {
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
        }

        public async Task<BaseResponse<IEnumerable<TemplateModel>>> GetTemplateModelsAsync()
        {
            using (var client = clientFactory.CreateClient())
            {
                SetupClient(client);
                string url = string.Format("https://www.LPK7.com/api/TemplateObjects/GetAllTemplates");

                var apiResponse = await client.GetAsync(url);
                if (!apiResponse.IsSuccessStatusCode)
                    return BadResponse<IEnumerable<TemplateModel>>.Create(ResponseStatus.InternalError);
                var resultStr = await apiResponse.Content.ReadAsStringAsync();
                var result = JsonConvert.DeserializeObject<IEnumerable<TemplateModel>>(resultStr);
                return SuccessResponse<IEnumerable<TemplateModel>>.Create(result);
            }
        }

        public async Task<BaseResponse> CreateSiteAsync(SiteViewModel site)
        {
            using (var client = clientFactory.CreateClient())
            {
                SetupClient(client);
                string url = string.Format("https://www.LPK7.com/api/SiteObjects/CreateSite?siteName={0}&templateName={1}&isEntireSitePublic={2}&requiresRegistration={3}&adminUserName={4}&adminEmail={5}&adminPassword={6}",
                    site.siteName, site.templateName, site.isEntireSitePublic, site.requiresRegistration, site.adminUserName, site.adminEmail, site.adminPassword
                );
                var apiResponse = await client.PostAsync(url, null);
                if (!apiResponse.IsSuccessStatusCode){
                    var response = await apiResponse.Content.ReadAsStringAsync();
                    return BadResponse.Create(ResponseStatus.InternalError, response);
                }
                return new BaseResponse();
            }
        }
    }
}