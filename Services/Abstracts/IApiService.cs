using System.Collections.Generic;
using System.Threading.Tasks;
using test_task.Models;
using test_task.Models.Infrastructure;

namespace test_task.Services.Abstracts
{
    public interface IApiService
    {
        Task<BaseResponse<IEnumerable<TemplateModel>>> GetTemplateModelsAsync();
        Task<BaseResponse> CreateSiteAsync(SiteViewModel site);
    }
}