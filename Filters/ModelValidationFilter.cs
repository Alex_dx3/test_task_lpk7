﻿using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using test_task.Models.Infrastructure;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace test_task.Filters
{
    public class ModelValidationFilter : Attribute, IActionFilter
    {
        public ModelValidationFilter()
        {
        }
        public void OnActionExecuted(ActionExecutedContext context)
        {
            if (context.Exception != null && context.Exception.GetType() == typeof(ValidationException))
            {
                var exception = context.Exception as ValidationException;
                var response = BadResponse<ModelValidationError[]>.Create(ResponseStatus.ValidationFailed);
                response.Data = new[]
                {
                    new ModelValidationError()
                    {
                        Error = exception.Message
                    }
                };
                context.Result = new Microsoft.AspNetCore.Mvc.JsonResult(response);
                context.ExceptionHandled = true;
            }
        }

        public void OnActionExecuting(ActionExecutingContext context)
        {
            if (context.ModelState.IsValid)
            {
                return;
            }
            var response = BadResponse<ModelValidationError[]>.Create(ResponseStatus.ValidationFailed);
            response.Data = context.ModelState.Keys
                .SelectMany(key => context.ModelState[key].Errors.Select(x => new ModelValidationError()
                {
                    Error = x.ErrorMessage,
                    Property = key
                })).ToArray();
            context.Result = new Microsoft.AspNetCore.Mvc.JsonResult(response);
        }
    }
}
