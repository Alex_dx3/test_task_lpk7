using System.ComponentModel.DataAnnotations;

namespace test_task.Models
{
    public class SiteViewModel
    {
        [Required]
        public string templateName { get; set; }
        [Required]
        public string siteName { get; set; }
        [Required]
        public bool isEntireSitePublic { get; set; }
        [Required]
        public bool requiresRegistration { get; set; }
        [Required]
        public string adminUserName { get; set; }
        [Required]
        public string adminEmail { get; set; }
        public string adminPassword { get; set; }
    }
}