namespace test_task.Models
{
    public class Description
    {
        public string Categories { get; set; }
        public string Keywords { get; set; }
        public string BrowserOrder { get; set; }
    }
    public class TemplateModel
    {
        public string Name { get; set; }
        public Description Description { get; set; }
        public string[] ThemeNames { get; set; }
    }
}