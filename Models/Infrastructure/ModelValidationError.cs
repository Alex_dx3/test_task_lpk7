﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace test_task.Models.Infrastructure
{
    public class ModelValidationError
    {
        public string Property { get; set; }
        public string Error { get; set; }
        public string ErrorCode { get; set; }
    }
}
