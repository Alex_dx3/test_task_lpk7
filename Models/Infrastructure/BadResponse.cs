﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace test_task.Models.Infrastructure
{
    [JsonConverter(typeof(StringEnumConverter))]
    public class BadResponse : BaseResponse
    {
        public string ErrorDescription { get; protected set; }
        private BadResponse(string errorDescription = null)
        {
            this.ErrorDescription = errorDescription;
            this.Status = ResponseStatus.InternalError;
            this.Message = Message ?? ErrorDescription;
        }
        private BadResponse(ResponseStatus status, string errorDescription = null)
        {
            this.Status = status;
            this.ErrorDescription = errorDescription ?? Status.ToString();
            this.Message = Message ?? ErrorDescription;
        }

        public static BadResponse Create(string errorDescription = null)
        {
            return new BadResponse(errorDescription);
        }
        public static BadResponse Create(ResponseStatus status, string errorDescription = null)
        {
            return new BadResponse(status, errorDescription);
        }
    }

    [JsonConverter(typeof(StringEnumConverter))]
    public class BadResponse<T> : BaseResponse<T>
    {
        public string ErrorDescription
        { get; protected set; }
        private BadResponse(string errorDescription = null)
        {
            this.Status = ResponseStatus.InternalError;

            this.ErrorDescription = errorDescription ?? Status.ToString();
            this.Message = Message ?? ErrorDescription;
        }
        private BadResponse(ResponseStatus status, string errorDescription = null)
        {
            this.Status = status;
            this.ErrorDescription = errorDescription ?? Status.ToString();
            this.Message = Message ?? ErrorDescription;
        }
        private BadResponse(ResponseStatus status, T data)
        {
            this.Status = status;
            this.Data = data;
            this.ErrorDescription = Status.ToString();
            this.Message = Message ?? ErrorDescription;
        }
        public static BadResponse<T> Create(string errorDescription = null)
        {
            return new BadResponse<T>(errorDescription);
        }
        public static BadResponse<T> Create(ResponseStatus status, string errorDescription = null)
        {
            return new BadResponse<T>(status, errorDescription);
        }
        public static BadResponse<T> Create(BaseResponse response)
        {
            var badresponse = (BadResponse)response;
            //var badresponse = (BadResponse)response;
            return new BadResponse<T>(badresponse.Status, badresponse.ErrorDescription);
        }
        public static BadResponse<T> Create(ResponseStatus status, T data)
        {
            return new BadResponse<T>(status, data);
        }
    }
}
