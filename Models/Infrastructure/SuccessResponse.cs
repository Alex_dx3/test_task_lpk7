﻿namespace test_task.Models.Infrastructure
{
    public class SuccessResponse<T> : BaseResponse<T>
    {
        private SuccessResponse(T data, string message = null)
        {
            this.Data = data;
            this.Message = message;
        }

        public static SuccessResponse<T> Create(T data, string message = null)
        {
            return new SuccessResponse<T>(data, message);
        }
    }
}
