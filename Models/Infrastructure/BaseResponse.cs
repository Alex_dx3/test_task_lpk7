﻿using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace test_task.Models.Infrastructure
{
    [JsonConverter(typeof(StringEnumConverter))]
    public class BaseResponse
    {
        /// <summary>
        /// Did request execute successfully
        /// </summary>
        public bool IsSuceceded => Status == ResponseStatus.Ok;

        public ResponseStatus Status { get; protected set; } = ResponseStatus.Ok;

        // public string Status { get { return this.StatusCode.ToString(); } }

        /// <summary>
        /// Error message
        /// </summary>
        public string Message { get; protected set; }

    }

    [JsonConverter(typeof(StringEnumConverter))]
    public class BaseResponse<T> : BaseResponse
    {
        public T Data { get; set; }
    }

    public enum ResponseStatus
    {
        Ok = 1,
        InternalError = 5,
        ValidationFailed
    }
}