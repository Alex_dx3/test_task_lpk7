namespace test_task.Models
{
    public class ApiOptions
    {
        public string clientID { get; set; }
        public string clientSecret { get; set; }
        public string TOKEN_ENDPOINT { get; set; }
        public string AUTHZ_ENDPOINT { get; set; }
    }
}