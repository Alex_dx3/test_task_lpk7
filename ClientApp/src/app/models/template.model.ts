export interface Description {
    categories: string;
    keywords: string;
    browserOrder: string;
}
export interface TemplateModel {
    name: string;
    description: Description;
    themeNames: string[];
}

export interface BaseResponse<T>{
    data: T;
    isSuceceded: boolean;
    message: string;
    status: number;
}