import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { BaseResponse, TemplateModel } from '../models/template.model';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private httpClient: HttpClient) { }

  public getTemplates(): Observable<TemplateModel[]> {
    return this.httpClient.get<BaseResponse<TemplateModel[]>>(`${environment.url}/api/GetTemplates`).pipe(
      map(response=> {
        if(response.isSuceceded){
          return response.data;
        }else{
          alert(`Error ${response.status}: ${response.message}`);
          return [];
        }
    }));
  }

  public createSite(site: any): Observable<BaseResponse<any>>{
    return this.httpClient.post<BaseResponse<any>>(`${environment.url}/api/CreateSite`, site);
  }
}
