import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import { TemplateModel } from 'src/app/models/template.model';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-template-select',
  templateUrl: './template-select.component.html',
  styleUrls: ['./template-select.component.css']
})
export class TemplateSelectComponent implements OnInit {
  @Input("form") form: FormGroup;
  templates$: Observable<TemplateModel[]>;
  constructor(private apiService: ApiService) { }

  ngOnInit() {
    this.templates$ = this.apiService.getTemplates();
  }

}
