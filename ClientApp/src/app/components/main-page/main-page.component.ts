import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, ParamMap, Route, Router } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { ApiService } from 'src/app/services/api.service';
@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.css']
})
export class MainPageComponent implements OnInit {
  form: FormGroup;
  loading = false;
  constructor(private apiService: ApiService) {
  }

  ngOnInit() {
    this.resetForm();
  }

  private resetForm(){
    this.form = new FormGroup({      
      templateName: new FormControl('', Validators.required),
      siteName: new FormControl('', Validators.required),
      isEntireSitePublic: new FormControl(true, Validators.required),
      requiresRegistration: new FormControl(true, Validators.required),
      adminUserName: new FormControl('', Validators.required),
      adminEmail: new FormControl('', Validators.required),
      adminPassword: new FormControl('')
    });
    this.form.markAsUntouched();
  }

  onSubmit(){
    if(this.form.invalid){
      this.form.markAllAsTouched();
    }else{
      this.loading = true;
      this.apiService.createSite(this.form.value).subscribe(response=> {
        this.loading = false;
        if(response.isSuceceded){
          this.resetForm();
          alert("Successfully created new site!");
        }else{
          if(response.status == 6){
            // let errors = response.data as any[]; //Don't want to spend time on it =)
            // errors.forEach(error=>{
            //   var cntrl = this.form.controls[error.Property];
            //   cntrl.setErrors({ custom: error.Error}, {emitEvent: true});
            // } );
            alert("Validation failed");
          }else{
            alert(`Error ${response.status}: ${response.message}`);
          }
        }
      }, err=> { alert(`Error on request: ${err}`);})
    }
  }
}
