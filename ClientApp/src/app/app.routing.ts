import { Route } from "@angular/router";
import { MainPageComponent } from "./components/main-page/main-page.component";

export const appRoutes: Route[] = [
    {
        path: '',
        pathMatch: 'full',
        component: MainPageComponent
    }
]